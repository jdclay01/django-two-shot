from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


admin.site.register(ExpenseCategory)
admin.site.register(Account)


class ReceiptAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)

# Register your models here.
